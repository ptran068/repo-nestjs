import * as dotenv from 'dotenv'
import * as Joi from 'joi'
import * as fs from 'fs'
import { extname } from 'path'
import { diskStorage } from 'multer'

export interface EnvConfig {
  [key: string]: string
}

export class ConfigService {
  private readonly envConfig: EnvConfig

  constructor() {
    let configFilePath = ''
    if (process.env.NODE_ENV && process.env.NODE_ENV === 'production' && !process.env.IS_BUILD) {
      configFilePath = '.production.env'
    }

    if (process.env.NODE_ENV && process.env.NODE_ENV !== 'production' || process.env.IS_BUILD) {
      configFilePath = '.development.env'
    }

    if (!process.env.NODE_ENV) {
      configFilePath = '.local.env'
    }

    const config = dotenv.parse(fs.readFileSync(configFilePath))
    config.NODE_ENV = process.env.NODE_ENV || 'local'
    config.IS_BUILD = process.env.IS_BUILD || 'false'
    console.log('Node env', process.env.NODE_ENV)
    this.envConfig = this.validateInput(config)
  }

  /**
   * Ensures all needed variables are set, and returns the validated JavaScript object
   * including the applied default values.
   */
  private validateInput(envConfig: EnvConfig): EnvConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      NODE_ENV: Joi.string()
        .valid(['development', 'production', 'test', 'provision', 'local'])
        .default('development'),
      PATIENTS_DATABASE_USER: Joi.string().required(),
      PATIENTS_DATABASE_PASSWORD: Joi.string().required(),
      PATIENTS_DATABASE_HOST: Joi.string().required(),
      PATIENTS_DATABASE_PORT: Joi.number().required(),
      PATIENTS_DATABASE_NAME: Joi.string().required(),
      MAIN_DATABASE_USER: Joi.string().required(),
      MAIN_DATABASE_PASSWORD: Joi.string().required(),
      MAIN_DATABASE_HOST: Joi.string().required(),
      MAIN_DATABASE_PORT: Joi.number().required(),
      MAIN_DATABASE_NAME: Joi.string().required(),
      EXPECTATIONS_DATABASE_USER: Joi.string().required(),
      EXPECTATIONS_DATABASE_PASSWORD: Joi.string().required(),
      EXPECTATIONS_DATABASE_HOST: Joi.string().required(),
      EXPECTATIONS_DATABASE_PORT: Joi.number().required(),
      EXPECTATIONS_DATABASE_NAME: Joi.string().required(),
      NOTIFICATIONS_DATABASE_USER: Joi.string().required(),
      NOTIFICATIONS_DATABASE_PASSWORD: Joi.string().required(),
      NOTIFICATIONS_DATABASE_HOST: Joi.string().required(),
      NOTIFICATIONS_DATABASE_PORT: Joi.number().required(),
      NOTIFICATIONS_DATABASE_NAME: Joi.string().required(),
      DONATES_DATABASE_USER: Joi.string().required(),
      DONATES_DATABASE_PASSWORD: Joi.string().required(),
      DONATES_DATABASE_HOST: Joi.string().required(),
      DONATES_DATABASE_PORT: Joi.number().required(),
      DONATES_DATABASE_NAME: Joi.string().required(),
      INTERNAL_HOST: Joi.string().required(),
      INTERNAL_PORT: Joi.number().required(),
      FRONT_END_HOST: Joi.string().required(),
      API_HOST: Joi.string().required(),
      JWT_SECRET: Joi.string().required(),
      BCRYPT_SALT: Joi.number().required(),
      REGEX: Joi.string().required(),
      JWT_COMFIRM_SECRET: Joi.string().required(),
      IS_BUILD: Joi.string().required(),
    })

    const { error, value: validatedEnvConfig } = Joi.validate(
      envConfig,
      envVarsSchema,
    )

    if (error) {
      throw new Error(`Config validation error: ${error.message}`)
    }

    const jwtComfirmSecret = String(validatedEnvConfig.JWT_COMFIRM_SECRET)
    const jwtSecret = String(validatedEnvConfig.JWT_SECRET)

    if (jwtComfirmSecret === jwtSecret) {
      throw new Error('Jwt secret key and jwt comfirm secret key must be different')
    }

    return validatedEnvConfig
  }

  get isBuild(): string {
    return String(process.env.IS_BUILD)
  }

  get nodeEnv(): string {
    return String(this.envConfig.NODE_ENV)
  }

  get internalHost(): string {
    return String(this.envConfig.INTERNAL_HOST)
  }

  get internalPort(): string {
    return String(this.envConfig.INTERNAL_PORT)
  }

  get frontendHost(): string {
    return String(this.envConfig.FRONT_END_HOST)
  }

  get apiHost(): string {
    return String(this.envConfig.API_HOST)
  }

  get jwtSecret(): string {
    return String(this.envConfig.JWT_SECRET)
  }

  get bcryptSalt(): number {
    return Number(this.envConfig.BCRYPT_SALT)
  }

  get mainDbURI(): string {
    // tslint:disable-next-line:max-line-length
    return `mongodb+srv://${this.envConfig.MAIN_DATABASE_USER}:${this.envConfig.MAIN_DATABASE_PASSWORD}@${this.envConfig.MAIN_DATABASE_HOST}/${this.envConfig.MAIN_DATABASE_NAME}?retryWrites=true&w=majority`
    // tslint:disable-next-line:max-line-length
    // return `mongodb://${this.envConfig.DATABASE_USER}:${this.envConfig.DATABASE_PASSWORD}@${this.envConfig.DATABASE_HOST}:${this.envConfig.DATABASE_PORT}/${this.envConfig.DATABASE_NAME}?authSource=admin`;
  }

  get patientsDbURI(): string {
    return `mongodb+srv://${this.envConfig.PATIENTS_DATABASE_USER}:${this.envConfig.PATIENTS_DATABASE_PASSWORD}@${this.envConfig.PATIENTS_DATABASE_HOST}/${this.envConfig.PATIENTS_DATABASE_NAME}?retryWrites=true&w=majority`
  }

  get notificationsDbURI(): string {
    return `mongodb+srv://${this.envConfig.NOTIFICATIONS_DATABASE_USER}:${this.envConfig.NOTIFICATIONS_DATABASE_PASSWORD}@${this.envConfig.NOTIFICATIONS_DATABASE_HOST}/${this.envConfig.NOTIFICATIONS_DATABASE_NAME}?retryWrites=true&w=majority`
  }

  get donatesDbURI(): string {
    return `mongodb+srv://${this.envConfig.DONATES_DATABASE_USER}:${this.envConfig.DONATES_DATABASE_PASSWORD}@${this.envConfig.DONATES_DATABASE_HOST}/${this.envConfig.DONATES_DATABASE_NAME}?retryWrites=true&w=majority`
  }

  get expectationsDbURI(): string {
    return `mongodb+srv://${this.envConfig.EXPECTATIONS_DATABASE_USER}:${this.envConfig.EXPECTATIONS_DATABASE_PASSWORD}@${this.envConfig.EXPECTATIONS_DATABASE_HOST}/${this.envConfig.EXPECTATIONS_DATABASE_NAME}?retryWrites=true&w=majority`
  }

  get getRegex(): string {
    return String(this.envConfig.REGEX)
  }

  get getJwtComfirmSecret(): string {
    return String(this.envConfig.JWT_COMFIRM_SECRET)
  }

  get multerOptions(): any {
    return {
      storage: diskStorage({
        destination: './uploads',
        filename: (req, file, cb) => {
          const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
          return cb(null, `${randomName}${extname(file.originalname)}`)
        },
      }),
    }
  }
}
