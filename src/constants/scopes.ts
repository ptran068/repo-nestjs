export enum scopes {
  PER_USER_READ = 'users:read',
  PER_USER_CREATE = 'users:create',
  PER_USER_UPDATE = 'users:update',
  PER_USER_DELETE = 'delete',
}
