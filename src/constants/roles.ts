import { scopes } from './scopes'

export const Roles = {
  BASIC: [
    scopes.PER_USER_CREATE,
    scopes.PER_USER_DELETE,
    scopes.PER_USER_READ,
    scopes.PER_USER_UPDATE,
  ],
}
