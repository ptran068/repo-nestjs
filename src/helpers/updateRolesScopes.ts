import * as mongodb from 'mongodb'
import { Roles } from '../constants/roles'
import { ConfigService } from '../configs/configs.service'

const configsService = new ConfigService()
const runningAsScript = require.main === module

export const updateRoleScopes = async (): Promise<any> => {
  try {
    const MongoClient = mongodb.MongoClient
    const conn = await MongoClient.connect(configsService.mainDbURI)
    const db = conn.db()
    const promises = Object.keys(Roles).map(async key => {
      const query = { roleName: key }
      const role = await db.collection('roles').findOne(query)

      if (!role) {
        await db.collection('roles').insertOne({
          roleName: key,
          scopes: [],
        })
      }

      return db.collection('roles').findOneAndUpdate(query, {
        $set: { scopes: Roles[key] },
      })
    })

    await Promise.all(promises)
  } catch (error) {
    return Promise.reject(error)
  }
}

if (runningAsScript) {
  updateRoleScopes()
    .then(() => {
      console.log('Done')
      process.exit(0)
    })
    .catch(e => console.log(e))
}
