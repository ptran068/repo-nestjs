import { InternalServerErrorException, BadRequestException, ForbiddenException, NotFoundException } from '@nestjs/common'
import { notFoundErrors, badRequestErrors, forbiddenErrors } from '../constants/errors'
import { ConfigService } from '../configs/configs.service'

const configService = new ConfigService()
const checkControllerErrorsInLocal = (error: any): void => {
  if (error.name === 'MongoError' && error.code === 11000) {
    console.log(`${error.message} - ${error.stack}`)
    throw new BadRequestException('Duplicated email, name or phone')
  }

  if (error.code === 403 || error.status === 403) {
    console.log(`${error.message} - ${error.stack}`)
    throw new ForbiddenException(forbiddenErrors[error.name])
  }

  if (error.code === 404 || error.status === 404) {
    console.log(`${error.message} - ${error.stack}`)
    throw new NotFoundException(notFoundErrors[error.name])
  }

  if (error.code === 400 || error.status === 400) {
    console.log(`${error.message} - ${error.stack}`)
    throw new BadRequestException(badRequestErrors[error.name])
  }

  console.log(`${error.message} - ${error.stack}`)

  throw new InternalServerErrorException({
    error: `${error.message} - ${error.stack}`,
  })
}

const checkControllerErrorsInProd = (error: any): void => {
  if (error.name === 'MongoError' && error.code === 11000) {
    throw new BadRequestException('Duplicated email, name or phone')
  }

  if (error.name === 'ValidationError' && error.code === 403) {
    throw new ForbiddenException('No permisisons')
  }

  if (error.code === 404) {
    throw new NotFoundException(notFoundErrors[error.name])
  }

  if (error.code === 400) {
    throw new BadRequestException(badRequestErrors[error.name])
  }

  throw new InternalServerErrorException('Something wrong happens')
}

export const checkControllerErrors = configService.nodeEnv !== 'production'
    ? checkControllerErrorsInLocal
    : checkControllerErrorsInProd
