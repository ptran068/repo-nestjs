import * as mongoose from 'mongoose'
import { ConfigService } from '../../configs/configs.service'
import { Module } from '@nestjs/common'

const configService = new ConfigService()
const MongooseProviders = [
  {
    provide: 'MainConnection',
    useFactory: (): Promise<typeof mongoose> =>
      mongoose.connect(configService.mainDbURI),
  },
  {
    provide: 'ExpectationsConnection',
    useFactory: (): Promise<typeof mongoose> =>
      mongoose.connect(configService.expectationsDbURI),
  },
  {
    provide: 'NotificationsConnection',
    useFactory: (): Promise<typeof mongoose> =>
      mongoose.connect(configService.notificationsDbURI),
  },
  {
    provide: 'DonatesConnection',
    useFactory: (): Promise<typeof mongoose> =>
      mongoose.connect(configService.donatesDbURI),
  },
  {
    provide: 'PatientsConnection',
    useFactory: (): Promise<typeof mongoose> =>
      mongoose.connect(configService.patientsDbURI),
  }
]

@Module({
  providers: MongooseProviders,
  exports: MongooseProviders
})
export class MongooseModule { }
