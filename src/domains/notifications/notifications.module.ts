import { Module } from '@nestjs/common'
import { NotificationsController } from './notifications.controller'
import { NotificationsService } from './notifications.service'
import { NotificationsSchema } from './types/notifications.schema'
import { MongooseModule } from '../mongoose/mongoose.module'
import { Connection } from 'mongoose'

const NotificationsProvide = [
  NotificationsService,
  {
    provide: 'NotificationsModel',
    useFactory: (connection: Connection) => connection.model('Notifications', NotificationsSchema),
    inject: ['NotificationsConnection'],
  },
]

@Module({
  imports: [
    MongooseModule,
  ],
  controllers: [NotificationsController],
  providers: NotificationsProvide,
  exports: NotificationsProvide,
})

export class NotificationsModule { }
