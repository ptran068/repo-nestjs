import * as mongoose from 'mongoose'
const { Types: { ObjectId } } = mongoose.Schema

export const NotificationsSchema = new mongoose.Schema({
  countryID: { type: ObjectId, required: false },
  provinceID: { type: ObjectId, required: false },
  title: { type: String, default: '' },
  body: { type: String, default: '' },
  clickAction: { type: String, default: '' },
  event: { type: String, default: '' },
  type: { type: String, enum: ['country', 'province', 'person'], default: 'person' },
  targetUID: { type: ObjectId, required: true },
  targetFirstName: String,
  targetLastName: String,
  targetProfilePhoto: String,
  isRead: { type: Boolean, default: false },
  createdBy: { type: ObjectId, required: false },
  createdByFirstName: String,
  createdByLastName: String,
  createdByProfilePhoto: String,
  createdAt: { type: Date, default: Date.now() },
  updatedAt: { type: Date, default: Date.now() }
}, {
  collation: { locale: 'en', strength: 2 },
})

NotificationsSchema.pre('findOneAndUpdate', function() {
  this.update({ }, { updatedAt: Date.now() })
})

NotificationsSchema.pre('updateMany', function() {
  this.update({ }, { updatedAt: Date.now() })
})

NotificationsSchema.pre('updateOne', function() {
  this.update({ }, { updatedAt: Date.now() })
})
