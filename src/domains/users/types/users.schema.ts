import * as mongoose from 'mongoose'
const { Types: { ObjectId } } = mongoose.Schema

export const UsersSchema = new mongoose.Schema({
  firstName: String,
  lastName: String,
  email: { type: String, required: true, unique: true, lowercase: true },
  phoneNumber: { type: String, required: false },
  password: { type: String, required: false },
  status: { type: String, enum: ['INACTIVE', 'ACTIVE'], default: 'INACTIVE' },
  providers: {
    googleID: { type: String, default: null },
  },
  profilePhoto: String,
  access: [{
    roleID: { type: ObjectId, ref: 'Roles', required: true },
    countryID: { type: ObjectId, ref: 'Countries', required: false },
    provinceID: { type: ObjectId, ref: 'Provinces', required: false }
  }],
  gender: { type: String, enum: ['MALE', 'FEMALE', 'OTHER'] },
  birthday: { type: Date, required: false },
  address: String,
  topics: [{ type: String, required: true }],
  oldPasswords: [{ type: String, required: true }],
  changePasswordAt: Number,
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
}, {
  collation: { locale: 'en', strength: 2 },
})

UsersSchema.pre('findOneAndUpdate', function() {
  this.update({ }, { updatedAt: Date.now() })
})

UsersSchema.pre('updateMany', function() {
  this.update({ }, { updatedAt: Date.now() })
})

UsersSchema.pre('updateOne', function() {
  this.update({ }, { updatedAt: Date.now() })
})
