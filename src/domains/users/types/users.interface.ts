import { Document, Types } from 'mongoose'

export interface User extends Document {
  firstName: string,
  lastName: string,
  email: string,
  phoneNumber: string,
  password: string,
  status: string,
  providers: Providers,
  profilePhoto: string,
  access: Access[],
  gender: string,
  birthday: Date,
  address: string,
  topics: string[],
  oldPasswords: string[],
  changePasswordAt: number,
  createdAt: Date,
  updatedAt: Date,
}

export interface CurrentUserCredentials extends Document {
  firstName: string,
  lastName: string,
  email: string,
  phoneNumber: string,
  password: string,
  status: string,
  providers: Providers,
  profilePhoto: string,
  access: Access[],
  gender: string,
  birthday: Date,
  address: string,
  topics: string[],
  oldPasswords: string[],
  changePasswordAt: number,
  createdAt: Date,
  updatedAt: Date,
}

export interface Providers {
  googleID: string
}

export interface Access {
  roleID: Types.ObjectId
  countryID: Types.ObjectId
  provinceID: Types.ObjectId
}

export interface GetCurrentUserCredentials {
  email?: string
  _id?: Types.ObjectId
  googleID?: string
}