import { Module } from '@nestjs/common'
import { UsersController } from './users.controller'
import { UsersService } from './users.service'
import { UsersSchema } from './types/users.schema'
import { Connection } from 'mongoose'
import { MongooseModule } from '../mongoose/mongoose.module'

const UsersProvide = [
  UsersService,
  {
    provide: 'UsersModel',
    useFactory: (connection: Connection) => connection.model('Users', UsersSchema),
    inject: ['MainConnection'],
  },
]

@Module({
  imports: [
    MongooseModule,
  ],
  controllers: [UsersController],
  providers: UsersProvide,
  exports: UsersProvide,
})

export class UsersModule { }
