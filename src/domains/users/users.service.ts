import { Injectable, Inject } from '@nestjs/common'
import { Model } from 'mongoose'
import { User, GetCurrentUserCredentials, CurrentUserCredentials } from './types/users.interface'

@Injectable()
export class UsersService {
  constructor(
    @Inject('UsersModel')
    private readonly usersModel: Model<User>,
  ) { }

  async getCurrentUserCredentials(query: GetCurrentUserCredentials): Promise<CurrentUserCredentials> {
    try {
      const user = await this.usersModel
        .findOne(query)
        .populate('access.roleID')

      return user
    } catch (error) {
      return Promise.reject(error)
    }
  }
}
