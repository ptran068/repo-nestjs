import { Module } from '@nestjs/common'
import { ConfigModule } from './configs/configs.module'
import { UsersModule } from './domains/users/users.module'
import { NotificationsModule } from './domains/notifications/notifications.module'
import { MongooseModule } from './domains/mongoose/mongoose.module'

@Module({
  imports: [
    ConfigModule,
    MongooseModule,
    UsersModule,
    NotificationsModule,
  ],
})

export class AppModule { }
