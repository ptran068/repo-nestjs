import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger'
import { ConfigService } from './configs/configs.service'
import { updateRoleScopes } from './helpers/updateRolesScopes'

const configService = new ConfigService()

async function bootstrap() {
  try {
    const app = await NestFactory.create(AppModule)
    app.enableCors()
    // build documents for APIs
    const options = new DocumentBuilder()
      .setTitle('Booth Genie')
      .setDescription(`The Booth Genie's APIs description`)
      .setVersion('1.0')
      .build()
    const document = SwaggerModule.createDocument(app, options)
    SwaggerModule.setup('api', app, document)

    if (configService.nodeEnv !== 'local' && configService.isBuild !== 'true') {
      await updateRoleScopes()
    }

    await app.listen(configService.internalPort, configService.internalHost)
    console.log(`our app started at ${configService.internalHost}:${configService.internalPort}`)
  } catch (error) {
    console.log(error)
  }
}

bootstrap()
