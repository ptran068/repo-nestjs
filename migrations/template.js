'use strict'
const mongodb = require('mongodb')
const url = `mongodb+srv://${process.env.DATABASE_USER}:${process.env.DATABASE_PASSWORD}@${process.env.DATABASE_HOST}/${process.env.DATABASE_NAME}?retryWrites=true&w=majority`
const MongoClient = mongodb.MongoClient

module.exports.up = function (next) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    //TODO
    db.close();
  });
  next()
}

module.exports.down = function (next) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    //TODO
    db.close();
  });
  next()
}
