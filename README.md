api-c19
## Description

- Main: dùng cho users, roles, countries và provinces
- Notifications: dùng để lưu trữ notifications
- Expectations: dùng để lưu trữ expectations và voted
- Patients: dùng để lưu trữ thông tin bệnh nhân, db này sẽ cũng được dùng ở serverless để cào dữ liệu về và lưu vàp
- Donates: dùng để lưu trữ thông tin của những người donates, đối với thằng này thì mình sẽ viết các controller thêm sửa xoá và lấy giữ liệu cho nó thôi


[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
```

* tslint extension might be good for you

## Running the app

```bash
# local
$ npm run start:local

# production mode
$ npm run start:prod
```

## Migration

```bash
# migrate roles' scopes
$ npm migrate:roles
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
## Swagger

- We need to take good care of swagger. Please follow it docs here
[https://docs.nestjs.com/recipes/swagger](https://docs.nestjs.com/recipes/swagger)

## Links
- Docs - [https://docs.nestjs.com](https://docs.nestjs.com/)

## Debug 
- configurations in Vscode, `launch.json` file
```bash
{
  "version": "0.2.0",
  "configurations": [
    {
      "name": "Current TS File",
      "type": "node",
      "request": "launch",
      "args": ["${workspaceFolder}/src/main.ts"],
      "runtimeArgs": [
        "--nolazy", 
        "-r",
        "ts-node/register",
        "-r",
        "tsconfig-paths/register"
      ],
      "sourceMaps": true,
      "cwd": "${workspaceRoot}",
      "protocol": "inspector",
      "env": {"NODE_ENV":"development"},
      "console": "integratedTerminal"
    }
  ]
}
```
