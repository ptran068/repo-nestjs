#!/bin/bash

if [ -z "$DEPLOY_AWS_PRIVATE_KEY" ]; then
  echo "DEPLOY_GIT_PRIVATE_KEY is not set"
  echo "Please set the DEPLOY_GIT_PRIVATE_KEY"
  exit 1
fi

apt-get update -y
apt install openssh-client -y
mkdir -p ~/.ssh
echo "$DEPLOY_AWS_PRIVATE_KEY" | tr -d '\r' > ~/.ssh/aws.pem
chmod 400 ~/.ssh/aws.pem
