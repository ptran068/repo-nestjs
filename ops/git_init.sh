#!/bin/bash

if [ -z "$DEPLOY_GIT_PRIVATE_KEY" ] || [ -z "$GIT_REPO" ]; then
  echo "DEPLOY_GIT_PRIVATE_KEY or GIT_REPO is not set"
  exit 1
fi

mkdir -p ~/.ssh
echo "$DEPLOY_GIT_PRIVATE_KEY" | tr -d '\r' > ~/.ssh/id_rsa
chmod 700 ~/.ssh/id_rsa
ssh-agent -h || ( yum -y install openssh-clients )
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_rsa
ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts
# git init
git checkout $BRANCH
git remote set-url origin $GIT_REPO
git config --global user.email "ci-cd@agent.com"
git config --global user.name "ci-cd-agent"
# git fetch origin $BRANCH

