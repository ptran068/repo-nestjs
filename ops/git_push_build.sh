#!/bin/bash
if [ -z "$BRANCH" ]; then
  echo "BRANCH not set"
  echo "Please set the GitLab branch name as BRANCH"
  exit 1
fi

git add .
git diff-index --quiet HEAD || ( git commit -m "[ci skip] update dist folder to prepare for deployment" -n && git push origin $BRANCH)
