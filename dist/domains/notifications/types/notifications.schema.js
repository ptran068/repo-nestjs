"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const { Types: { ObjectId } } = mongoose.Schema;
exports.NotificationsSchema = new mongoose.Schema({
    firstName: String,
    lastName: String,
    email: { type: String, required: true, unique: true, lowercase: true },
    phoneNumber: { type: String, required: false },
    password: { type: String, required: false },
    status: { type: String, enum: ['INACTIVE', 'ACTIVE'], default: 'INACTIVE' },
    providers: {
        googleID: { type: String, default: null },
    },
    profilePhoto: String,
    access: [{
            roleID: { type: ObjectId, ref: 'Roles', required: true },
            countryID: { type: ObjectId, ref: 'Countries', required: false },
            provinceID: { type: ObjectId, ref: 'Provinces', required: false }
        }],
    gender: { type: String, enum: ['MALE', 'FEMALE', 'OTHER'] },
    birthday: { type: Date, required: false },
    address: String,
    topics: [{ type: String, required: true }],
    oldPasswords: [{ type: String, required: true }],
    changePasswordAt: Number,
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
}, {
    collation: { locale: 'en', strength: 2 },
});
exports.NotificationsSchema.pre('findOneAndUpdate', function () {
    this.update({}, { updatedAt: Date.now() });
});
exports.NotificationsSchema.pre('updateMany', function () {
    this.update({}, { updatedAt: Date.now() });
});
exports.NotificationsSchema.pre('updateOne', function () {
    this.update({}, { updatedAt: Date.now() });
});
//# sourceMappingURL=notifications.schema.js.map