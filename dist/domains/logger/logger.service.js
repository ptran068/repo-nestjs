"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const winston_1 = require("winston");
exports.logTransportConsole = new winston_1.transports.Console({
    handleExceptions: true,
    format: winston_1.format.combine(winston_1.format.timestamp(), winston_1.format.printf((info) => {
        let log = '';
        if (info.timestamp) {
            log += `${info.timestamp} `;
        }
        if (info.meta && info.meta.context) {
            log += `${info.meta.context} `;
        }
        if (info.level) {
            log += `[${info.level.toLocaleUpperCase()}] `;
        }
        if (info.message) {
            log += `${info.message}`;
        }
        return log;
    })),
});
class MyLogger {
    constructor(context) {
        this.contextName = context;
        this.logger = winston_1.createLogger();
        this.logger.configure({
            transports: [
                exports.logTransportConsole,
            ],
            exitOnError: false,
        });
    }
    configure(configuration, contextName) {
        this.logger.configure(configuration);
        this.contextName = contextName ? contextName : this.contextName;
    }
    log(message) {
        this.logger.log({ level: 'info', message, meta: { context: this.contextName } });
    }
    error(message, stackTrace) {
        this.logger.log({ level: 'error', message, meta: { context: this.contextName, stackTrace } });
    }
    warn(message) {
        this.logger.log({ level: 'warn', message, meta: { context: this.contextName } });
    }
    info(message) {
        this.logger.log({ level: 'info', message, meta: { context: this.contextName } });
    }
}
exports.MyLogger = MyLogger;
//# sourceMappingURL=logger.service.js.map