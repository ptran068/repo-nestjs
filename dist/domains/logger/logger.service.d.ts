import { LoggerService } from '@nestjs/common';
import { LoggerOptions, transports } from 'winston';
export declare const logTransportConsole: transports.ConsoleTransportInstance;
export declare class MyLogger implements LoggerService {
    private readonly logger;
    private contextName;
    constructor(context: string);
    configure(configuration: LoggerOptions, contextName?: string): void;
    log(message: string): void;
    error(message: string, stackTrace?: any): void;
    warn(message: string): void;
    info(message: string): void;
}
