"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const configs_service_1 = require("../../configs/configs.service");
const common_1 = require("@nestjs/common");
const configService = new configs_service_1.ConfigService();
const MongooseProviders = [
    {
        provide: 'MainConnection',
        useFactory: () => mongoose.connect(configService.mainDbURI),
    },
    {
        provide: 'ExpectationsConnection',
        useFactory: () => mongoose.connect(configService.expectationsDbURI),
    },
    {
        provide: 'NotificationsConnection',
        useFactory: () => mongoose.connect(configService.notificationsDbURI),
    },
    {
        provide: 'DonatesConnection',
        useFactory: () => mongoose.connect(configService.donatesDbURI),
    },
    {
        provide: 'PatientsConnection',
        useFactory: () => mongoose.connect(configService.patientsDbURI),
    }
];
let MongooseModule = class MongooseModule {
};
MongooseModule = __decorate([
    common_1.Module({
        providers: MongooseProviders,
        exports: MongooseProviders
    })
], MongooseModule);
exports.MongooseModule = MongooseModule;
//# sourceMappingURL=mongoose.module.js.map