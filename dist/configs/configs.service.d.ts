export interface EnvConfig {
    [key: string]: string;
}
export declare class ConfigService {
    private readonly envConfig;
    constructor();
    private validateInput;
    get isBuild(): string;
    get nodeEnv(): string;
    get internalHost(): string;
    get internalPort(): string;
    get frontendHost(): string;
    get apiHost(): string;
    get jwtSecret(): string;
    get bcryptSalt(): number;
    get mainDbURI(): string;
    get patientsDbURI(): string;
    get notificationsDbURI(): string;
    get donatesDbURI(): string;
    get expectationsDbURI(): string;
    get getRegex(): string;
    get getJwtComfirmSecret(): string;
    get multerOptions(): any;
}
