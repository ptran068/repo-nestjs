export declare const notFoundErrors: {};
export declare const badRequestErrors: {};
export declare const forbiddenErrors: {
    ValidationError: string;
};
