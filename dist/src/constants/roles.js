"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const scopes_1 = require("./scopes");
exports.Roles = {
    BASIC: [
        scopes_1.scopes.PER_USER_CREATE,
        scopes_1.scopes.PER_USER_DELETE,
        scopes_1.scopes.PER_USER_READ,
        scopes_1.scopes.PER_USER_UPDATE,
    ],
};
//# sourceMappingURL=roles.js.map