"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv = require("dotenv");
const Joi = require("joi");
const fs = require("fs");
const path_1 = require("path");
const multer_1 = require("multer");
class ConfigService {
    constructor() {
        let configFilePath = '';
        if (process.env.NODE_ENV && process.env.NODE_ENV === 'production' && !process.env.IS_BUILD) {
            configFilePath = '.production.env';
        }
        if (process.env.NODE_ENV && process.env.NODE_ENV !== 'production' || process.env.IS_BUILD) {
            configFilePath = '.development.env';
        }
        if (!process.env.NODE_ENV) {
            configFilePath = '.local.env';
        }
        const config = dotenv.parse(fs.readFileSync(configFilePath));
        config.NODE_ENV = process.env.NODE_ENV || 'local';
        config.IS_BUILD = process.env.IS_BUILD || 'false';
        console.log('Node env', process.env.NODE_ENV);
        this.envConfig = this.validateInput(config);
    }
    validateInput(envConfig) {
        const envVarsSchema = Joi.object({
            NODE_ENV: Joi.string()
                .valid(['development', 'production', 'test', 'provision', 'local'])
                .default('development'),
            DATABASE_USER: Joi.string().required(),
            DATABASE_PASSWORD: Joi.string().required(),
            DATABASE_HOST: Joi.string().required(),
            DATABASE_PORT: Joi.number().required(),
            DATABASE_NAME: Joi.string().required(),
            INTERNAL_HOST: Joi.string().required(),
            INTERNAL_PORT: Joi.number().required(),
            FRONT_END_HOST: Joi.string().required(),
            API_HOST: Joi.string().required(),
            JWT_SECRET: Joi.string().required(),
            BCRYPT_SALT: Joi.number().required(),
            REGEX: Joi.string().required(),
            JWT_COMFIRM_SECRET: Joi.string().required(),
            IS_BUILD: Joi.string().required(),
        });
        const { error, value: validatedEnvConfig } = Joi.validate(envConfig, envVarsSchema);
        if (error) {
            throw new Error(`Config validation error: ${error.message}`);
        }
        const jwtComfirmSecret = String(validatedEnvConfig.JWT_COMFIRM_SECRET);
        const jwtSecret = String(validatedEnvConfig.JWT_SECRET);
        if (jwtComfirmSecret === jwtSecret) {
            throw new Error('Jwt secret key and jwt comfirm secret key must be different');
        }
        return validatedEnvConfig;
    }
    get isBuild() {
        return String(process.env.IS_BUILD);
    }
    get nodeEnv() {
        return String(this.envConfig.NODE_ENV);
    }
    get internalHost() {
        return String(this.envConfig.INTERNAL_HOST);
    }
    get internalPort() {
        return String(this.envConfig.INTERNAL_PORT);
    }
    get frontendHost() {
        return String(this.envConfig.FRONT_END_HOST);
    }
    get apiHost() {
        return String(this.envConfig.API_HOST);
    }
    get jwtSecret() {
        return String(this.envConfig.JWT_SECRET);
    }
    get bcryptSalt() {
        return Number(this.envConfig.BCRYPT_SALT);
    }
    get dbURI() {
        return `mongodb+srv://${this.envConfig.DATABASE_USER}:${this.envConfig.DATABASE_PASSWORD}@${this.envConfig.DATABASE_HOST}/${this.envConfig.DATABASE_NAME}?retryWrites=true&w=majority`;
    }
    get getRegex() {
        return String(this.envConfig.REGEX);
    }
    get getJwtComfirmSecret() {
        return String(this.envConfig.JWT_COMFIRM_SECRET);
    }
    get multerOptions() {
        return {
            storage: multer_1.diskStorage({
                destination: './uploads',
                filename: (req, file, cb) => {
                    const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('');
                    return cb(null, `${randomName}${path_1.extname(file.originalname)}`);
                },
            }),
        };
    }
}
exports.ConfigService = ConfigService;
//# sourceMappingURL=configs.service.js.map