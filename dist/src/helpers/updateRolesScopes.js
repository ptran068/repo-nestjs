"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongodb = require("mongodb");
const roles_1 = require("../constants/roles");
const configs_service_1 = require("../configs/configs.service");
const configsService = new configs_service_1.ConfigService();
const runningAsScript = require.main === module;
exports.updateRoleScopes = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const MongoClient = mongodb.MongoClient;
        const conn = yield MongoClient.connect(configsService.dbURI);
        const db = conn.db();
        const promises = Object.keys(roles_1.Roles).map((key) => __awaiter(void 0, void 0, void 0, function* () {
            const query = { roleName: key };
            const role = yield db.collection('roles').findOne(query);
            if (!role) {
                yield db.collection('roles').insertOne({
                    roleName: key,
                    scopes: [],
                });
            }
            return db.collection('roles').findOneAndUpdate(query, {
                $set: { scopes: roles_1.Roles[key] },
            });
        }));
        yield Promise.all(promises);
    }
    catch (error) {
        return Promise.reject(error);
    }
});
if (runningAsScript) {
    exports.updateRoleScopes()
        .then(() => {
        console.log('Done');
        process.exit(0);
    })
        .catch(e => console.log(e));
}
//# sourceMappingURL=updateRolesScopes.js.map