"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const errors_1 = require("../constants/errors");
const configs_service_1 = require("../configs/configs.service");
const configService = new configs_service_1.ConfigService();
const checkControllerErrorsInLocal = (error) => {
    if (error.name === 'MongoError' && error.code === 11000) {
        console.log(`${error.message} - ${error.stack}`);
        throw new common_1.BadRequestException('Duplicated email, name or phone');
    }
    if (error.code === 403 || error.status === 403) {
        console.log(`${error.message} - ${error.stack}`);
        throw new common_1.ForbiddenException(errors_1.forbiddenErrors[error.name]);
    }
    if (error.code === 404 || error.status === 404) {
        console.log(`${error.message} - ${error.stack}`);
        throw new common_1.NotFoundException(errors_1.notFoundErrors[error.name]);
    }
    if (error.code === 400 || error.status === 400) {
        console.log(`${error.message} - ${error.stack}`);
        throw new common_1.BadRequestException(errors_1.badRequestErrors[error.name]);
    }
    console.log(`${error.message} - ${error.stack}`);
    throw new common_1.InternalServerErrorException({
        error: `${error.message} - ${error.stack}`,
    });
};
const checkControllerErrorsInProd = (error) => {
    if (error.name === 'MongoError' && error.code === 11000) {
        throw new common_1.BadRequestException('Duplicated email, name or phone');
    }
    if (error.name === 'ValidationError' && error.code === 403) {
        throw new common_1.ForbiddenException('No permisisons');
    }
    if (error.code === 404) {
        throw new common_1.NotFoundException(errors_1.notFoundErrors[error.name]);
    }
    if (error.code === 400) {
        throw new common_1.BadRequestException(errors_1.badRequestErrors[error.name]);
    }
    throw new common_1.InternalServerErrorException('Something wrong happens');
};
exports.checkControllerErrors = configService.nodeEnv !== 'production'
    ? checkControllerErrorsInLocal
    : checkControllerErrorsInProd;
//# sourceMappingURL=checkErrors.js.map