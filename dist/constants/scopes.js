"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var scopes;
(function (scopes) {
    scopes["PER_USER_READ"] = "users:read";
    scopes["PER_USER_CREATE"] = "users:create";
    scopes["PER_USER_UPDATE"] = "users:update";
    scopes["PER_USER_DELETE"] = "delete";
})(scopes = exports.scopes || (exports.scopes = {}));
//# sourceMappingURL=scopes.js.map